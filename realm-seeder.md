---
title : "Event Sourcing PoC event seeder"
description: "Documentation for the SDG event seeder"
date: 2023-09-06T12:21:55+02:00
draft: false
toc: true
---
This application can be used, to create an initial state, which in turn simulators can consume.

## How to run
In order for the `seed_realm` binary to be able to publish synthetic events to a Jetstream, a nats endpoint is required, and an amount of instances to create per object. This can be done using the following ENV vars:

- `NATS_ENDPOINT`
- `NUM_INSTANCES`

Alternatively the following CLI args can be used:  
```
Usage: seed_realm [OPTIONS]

Options:
      --nats-endpoint <NATS_ENDPOINT>  NATS endpoint to publish to [default: nats://nats:4222]
      --num-instances <NUM_INSTANCES>  Amount of instances to spawn per type of object [default: 100000]
      --seed <SEED>                    Base64 encoded seed to be used by PRNG
```

The latter is optionally.

## Development
The following tools are required to be able to develop on this project:

- [Rust development environment](https://rust-lang.github.io/rustup/installation/index.html)
- [cargo-watch](https://github.com/watchexec/cargo-watch)
- [cargo-udeps](https://github.com/est31/cargo-udeps)

A Makefile has been created, which contains the following targets:
  - check: verify if required tools are available.
  - build: build the event seeder.
  - watch: build & execute the event seeder, and re[execute|build] it in case a change has been made to the source code.
  - container: build a container, and tag it as `sythentic-data-generator/seed-realm:latest`.
  - udeps: find unused dependencies.
  - clippy: run linter.
