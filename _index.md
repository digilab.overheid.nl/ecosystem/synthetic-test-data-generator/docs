---
title : "Synthetic Data Generator"
description: "General information regarding the Sythentic Data Generator"
lead: "PoC that showcases a system that's dedicated to the generation of synthetic governmental data."
date: 2023-09-06T12:21:55+02:00
draft: false
toc: true
---

# Documentation for the Digilab Synthetic Test Data Generator

## Rationale

For testing various software products of the Dutch Government and their integration, it's useful to have consistent test data representative of production data, but does not contain any potentially privacy-sensitive or otherwise confidential data.
For that purpose, we need a tool that allows generating realistic but fake data of various types, to be consumed by testing and acceptance environments of various products.

## High level design

We follow the event sourcing pattern for creating our data. What this means, is that we don't store entities as-is, but only as a sequence of mutation events.

The basic architecture is as follows:

![alt text](Architecture.svg "General architecture overview")

For a variety of business domains (eg. parking license or cadastral registrations), we define entities in terms of the events that lead to their present state. These events are then published to a messaging system, and consumed by a variety of event interpreters that translate the events to vendor-specific ways to populate their vendor's database.

We provide an initial collection of entities, in the form of events that directly lead to their initial existence (possibly using events that are only intended for this initialization). After that, each business domain has a simulator that generates realistic events, at a statistically realistic rate (but possibly accelerated by a configurably constant).


## Detailed design

The system has the following components:

### Initial Data Seed Generator

For each domain, we start out with some sensible collections of entities that "just exist". These take the form of events that declare the administrative existence, rather that real-world events like births.

### Event Simulator

For each domain, the simulator component has code to generate events per "clock tick".

The simulator consumes event messages from the Data Seed Generator to get to an initial state for all entities.

TODO: describe the simulator in more detail.

### Event Log

We use a messaging system with persistence (NATS JetStream) to publish and handle the events.

The event simulator(s) publish to event-specific subjects, namespaced by business domain and entity/aggregate type, eg. `events.frp.persoon.GeboorteVastgesteld`.
The event contains all meta-information describing the event, and an `eventData` key containing event type-specific data in JSON format.

The event interpreters subscribe to all subjects they're interested in, by prefix (eg. `events.frp.>` or `events.frp.persoon.*`)

### Event Interpreters

Event interpreters are vendor-specific subscribers that do whatever needs to be done to populate their vendor's database, eg. using a REST API, direct database statements, or whatever way the vendor's product allows automated data entry.


## Naming things

### Event format
We use JSON with `lowerCamelCase` keys in events, and `UpperCamelCase` for event types and other enum-like fields, eg.
```json
{
  "event": {
    "occurredAt": "1995-05-09T14:30:00Z",
    "registeredAt": "1995-05-09T14:30:00Z",
    "subjectIds": [
      "07996c80-8c6f-4021-a90d-b687cf928ca0"
    ],
    "eventType": "GeboorteVastgesteld",
    "eventData": {
      "someEventTypeSpecificData": [
        "inWhatever",
        {
          "dataStructure": "That makes sense for it"
        }
      ]
    }
  }
}
```

### Event types

For the first version we'll implement the following (fictitious) event types, each with their own NATS subject:

- personen ("Fictief Register Personen"):
  - `GeboorteVastgesteld` => `events.frp.persoon.GeboorteVastgesteld`
  - `NamenVastgesteld` => `events.frp.persoon.NamenVastgesteld`
  - `AdresIngeschreven` => `events.frp.persoon.AdresIngeschreven`
  - `AdresUitgeschreven` => `events.frp.persoon.AdresUitgeschreven`
  - `OverlijdenVastgesteld` => `events.frp.persoon.OverlijdenVastgesteld`
- kadaster ("Fictief Register Kadaster"):
  - `PerceelGeregistreerd` => `events.frk.perceel.Geregistreerd`
  - `PerceelEigenaarsVastgesteld` => `events.frk.perceel.EigenaarsVastgesteld`
  - `PercelenHerverkaveld` => `events.frk.perceel.Herverkaveld`
- voertuigen ("Fictief Register Voertuigen"):
  - `VoertuigGeregistreerd` => `events.frv.voertuig.Geregistreerd`
  - `VoertuigEigenaarVastgesteld` => `events.frv.voertuig.EigenaarVastgesteld`


## Development

### External documentation

- [Useful overview of the event sourcing pattern](https://martinfowler.com/eaaDev/EventSourcing.html).
- [Detailed explanation of how NATS JetStream works](https://docs.nats.io/nats-concepts/jetstream).
  - Of particular note is how [Consumers](https://docs.nats.io/nats-concepts/jetstream/consumers) work.
